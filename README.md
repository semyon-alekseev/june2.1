# The task is to write a .gitlab-ci with a docker-compose, which includes three services:
- nginx
- postgresql
- app (docker file with dependencies installing)
***
additional notes:
- nginx port $port:8000, where port variable depends on a used branch - dev or master
- builded images must have their own tag - app_dev and app_prod
- builded images are stored in our registry

# The result
- images' building is successful and the containers are running at the result
- we have the running app on the right port
- we have the builded images in our registry
